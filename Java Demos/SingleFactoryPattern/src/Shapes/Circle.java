package Shapes;

import Program.Shape;

// a concrete product of Shape
public class Circle implements Shape{

	private double radius;
	private String name;
	
	public Circle(double r, String n)
	{
		radius = r;
		name = n;
	}
	
	@Override
	public double getArea() {
		return Math.PI * Math.pow(radius, 2);
	}

	@Override
	public String getName() {
		return name;
	}
	
}
