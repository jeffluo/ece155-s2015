/*  Author: Jeff Y Luo
    For ECE 155 - Spring 2015
    University of Waterloo
    Department of Electrical and Computer Engineering */

package ca.uwaterloo.jeff.jokecheckbox;

import ca.uwaterloo.jeff.jokecheckbox.R;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

public class MainActivity extends Activity {

	int counter = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// get a reference to each of the button UI objects
		Button smallButton = (Button) findViewById(R.id.button1);
		Button largeButton = (Button) findViewById(R.id.button2);

		// get a reference to the upper check box object
		final CheckBox cb = (CheckBox) findViewById(R.id.checkBox1);

		// register & bind click event with first button
		smallButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) { // onClick event handler
				// --- find the text view --
				TextView txtView = (TextView) findViewById(R.id.textView1);
				// -- change text size to something small --
				txtView.setTextSize(12);
			}
		});

		// register & bind click event with second button
		largeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// --- find the text view --
				TextView txtView = (TextView) findViewById(R.id.textView1);
				// -- change text size to something big --
				txtView.setTextSize(24);
			}
		});

		// useless code example: event handler for upper check box
		// this event activates whenever the checkbox's "checked" status changes (ie the user checks or unchecks the box)
		// the handler sets the checked status back to false, in essence unchecking the box...
		cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked) {
					cb.setChecked(false);
					cb.setText("checkbox checked " + ++counter + " time(s)");
				}
			}
		});
	}
	
	
}
