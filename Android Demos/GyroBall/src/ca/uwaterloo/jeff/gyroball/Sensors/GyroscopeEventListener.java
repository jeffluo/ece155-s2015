/*  Author: Jeff Y Luo
    For ECE 155 - Spring 2015
    University of Waterloo
    Department of Electrical and Computer Engineering */

package ca.uwaterloo.jeff.gyroball.Sensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.util.Log;
import android.widget.TextView;

public class GyroscopeEventListener implements SensorEventListener {
	private TextView textView_x;
	private TextView textView_y;
	private TextView textView_z;

	private static float latestReading_x;
	private static float latestReading_y;
	private static float latestReading_z;

	private static GyroscopeEventListener singleton;

	private GyroscopeEventListener(TextView x, TextView y, TextView z)
	{
		// store local references to the 3 text views
		textView_x = x;
		textView_y = y;
		textView_z = z;

		// set default reading values
		latestReading_x = 0;
		latestReading_y = 0;
		latestReading_z = 0;
	}

	public static GyroscopeEventListener getListener(TextView x, TextView y, TextView z)
	{
		if(singleton == null)
		{
			singleton = new GyroscopeEventListener(x, y, z);
		}
		return singleton;
	}	

	public static void removeListener()
	{
		singleton = null;
	}

	@Override	// required to override, we do nothing here
	public void onAccuracyChanged(Sensor sensor, int accuracy) {}

	@Override
	public void onSensorChanged(SensorEvent event)
	{
		if(event.sensor.getType() == Sensor.TYPE_GYROSCOPE)
		{
			// if sensor type is gyroscope, then update our latest readings
			latestReading_x += event.values[0];
			latestReading_y += event.values[1];
			latestReading_z += event.values[2];

			// update text labels only if they've been given
			// in Game Activity we don't have a z axis label (since it's irrelevant)
			if(textView_x != null)
			{
				textView_x.setText("x: " + String.format("%.3f", latestReading_x));
			}
			if(textView_y != null)
			{
				textView_y.setText("y: " + String.format("%.3f", latestReading_y));
			}
			if(textView_z != null)
			{
				textView_z.setText("z: " + String.format("%.3f", latestReading_z));
			}
		}
	}

	// Public access for the latest reading values
	public float getX()
	{
		return latestReading_x;
	}
	public float getY()
	{
		return latestReading_y;
	}
	public float getZ()
	{
		return latestReading_z;
	}

	// Reset all gyroscope positions to 0
	public void reset()
	{
		latestReading_x = 0;
		latestReading_y = 0;
		latestReading_z = 0;
		Log.d("Brick", "resetting Gyro readings: " + String.format("%.3f %.3f %.3f", latestReading_x, latestReading_y, latestReading_z));
	}
}
