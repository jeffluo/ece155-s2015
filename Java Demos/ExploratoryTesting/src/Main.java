// Don't look at the source code while doing exploratory testing!




















































































import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class Main {
	public static void main(String[] args) throws IOException{
		boolean exitProgram = false;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		while(!exitProgram) {
			System.out.print("Input:");
			String input = br.readLine();
			switch(input.charAt(0)){
			case 'Q':
				System.out.println("Output: Quitting!");
				exitProgram = true;
				break;
			case 'A':
				System.out.println("Output: Wow you're beautiful!");
				break;
			case '1':
				System.out.println("Output: [0]");
				break;
			case '2':
				System.out.println("Output: [0, 1]");
				break;
			default :
				int x = Integer.parseInt(input);
				ArrayList<Integer> out = new ArrayList<Integer>();
				for(int i = 0; i < x; i++) {
					out.add(i);
				}
				System.out.println("Output: " + out);
				break;
			}
		}
	}
}
