/*  Author: Jeff Y Luo
    For ECE 155 - Spring 2015
    University of Waterloo
    Department of Electrical and Computer Engineering */

package ca.uwaterloo.jeff.gyroball;

import android.app.ActionBar;
import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;
import ca.uwaterloo.jeff.gyroball.R;
import ca.uwaterloo.jeff.gyroball.Sensors.GyroscopeEventListener;

public class GameActivity extends Activity {
	private ActionBar actionBar;

	private SensorManager sensorManager;
	private Sensor gyroscope;
	private SensorEventListener gyroListener;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);

		// change the action bar
		actionBar = this.getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);	// show back arrow
		actionBar.setDisplayShowHomeEnabled(false);	// hide icon

		// gyro data read out text labels
		TextView gyro_x = (TextView) this.findViewById(R.id.game_gyroscope_textview_x);
		TextView gyro_y = (TextView) this.findViewById(R.id.game_gyroscope_textview_y);
		
		// set up gyroscope
		sensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);
		gyroscope = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
		GyroscopeEventListener.removeListener();
		gyroListener = GyroscopeEventListener.getListener(gyro_x, gyro_y, null);
		sensorManager.registerListener(gyroListener, gyroscope, SensorManager.SENSOR_DELAY_GAME);
	}
}
