package Program;

// Shape is our abstract product which the ShapeFactory creates
// the concrete product is any class which implements the Shape class, in this demo it is either Circle or Square.
public interface Shape {
	public double getArea();
	public String getName();
}
